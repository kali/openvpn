Running a shapeshifter-dispatcher container in float
---

Chaperone, a small init, is used to launch shapeshifter dispatcher. It needs to have some variables:

* RHOST: where traffic should go to, like the IP of where your OpenVPN server
  listens on. If it's on the same host 127.0.0.1 would do.
* RPORT: associated port of that OpenVPN server, 1194 by default
* LHOST: listen on this IP 
* OBFSPORT: your obfs4-exposed port
* LOGLEVEL: ERROR/WARN/INFO/DEBUG
* EXTORPORT: Specify the address of a server implementing the Extended OR Port protocol, which is used for per-connection metadata

